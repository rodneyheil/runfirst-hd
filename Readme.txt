History:
7.0 10/30/2018 - Rodney Heil
- Added error message about spaces as delimiters

6.1b: 12/4/15 - Lance Hunter
- Included CM570 as C6 correctly

6.1a: 11/4/15 - Lance Hunter
- Included CM570 as C6

6.1 Includes floating point add, rep, & percent(using signed ints)
Added CM2350



5.4 - Added tune.  now there is Econ, Econ/Power, and Power.
Map levels:	1 - Economy only
00000X00	2 - Economy with Power only
		3 - Econ + Econ/Power
		4 - Power only
		5 - Econ + Power
		6 - Econ/Power + Power
		7 - All; Econ + Econ/Power + Power

5.3.2
Increased curves another 20% JM

5.3.1
 - Doubled the Curves = JM

5.3.0d 8/13/13 - LH
 - Updated 0746002C to have right number of economy finds (B to A).

5.3.0c 8/1/13 - LH
 - Updated correct address for timing maps in 0746002B

5.3.0b 7/22/13 - LH
 - part # 746002C changed number of econ maps to be found from B to A

5.3.0 6/18/13 - LH
 - Added tuning by address
 - Added tuning by operating system #
- TIM1 of 7460004 is wrong, so took it out.
~~~~~~~~~~~~~~~~~~~
 
5.2.4: 6/5/13 - LH
- Added Pulse width table for #746002C

5.2.3: 5/15/13 LH
//Only for Calibration # 746001F
- Added another SOI table
- Removed Main Source of injection table
- Added pulse width back in.

5.2.2
- Added 7460016

5.2.2b: 5/7/13 LH
- Modified 746001B for correct map address (Timing 3)

5.2.1b: 5/2/13 LH
- Modified 746002C for correct number of maps

5.2.1: 5/1/13 LH
- Added 746002C

5.2.0: 4/29/13 LH
- Added # of maps to search for in partnumbers.txt
- Added 746002B and 7280006

5.1: 1 - economy change only, 2 - Power only, 3 - map is changed in both selections.

5.0:	- modified IDs to ADD and REPLACE, included PERCENT changes.
	- can search by search string or address

~~~~~~~~~~~~~~~~~~

4.0.4 - 11/29/2012	
- increase fuel on low end 
4.0.3 - 8/16/12
- double 4.0.2 for gord spears extreme tuning
4.0.2 - 8/1/12
- doubled for gord spears extreme tune
4.0.1 - 4/23/12
- Added new pulse width map search string for CL10138.15
4.0 - 3/13/12
- Find search string -- has number of maps to find for economy and then power.
nibble 4 map header ID is if map is for econ  or power/econ
- Word/Integer maps now integers, not bytes.
- Added fuel pulse width table

~~~~~~~~~~~~~~~~~~~~

3.0 CMNS DPF: 2/24/12
- Version data has changed: 2.0 = CM2250 2.0	3.0 = 3.0 CMNS SCR

~~~~~~~~~~~~~~~~~~~~~

2.2 - Turned up maps to max of 1900, both.
2.1 - shortened fuel search strings, changed map to add(up to 1600/1700) instead of replace
2.0 - new tuning style


//////////////////////////////////////////////////////////////////////////////////
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//////////////////////////////////////////////////////////////////////////////////


---v5.3 changes: 5/24/13

Added:
DDEC10	D1
DDEC13	D3
Paccar	P1
Maxxforce	M1
MaxxForce	M2

Changed code to allow for comments better in partnumbers.txt


---v5.2 changes: 4/18/2013

removed 'Find' in Search string file for number of maps to find
Now each part in the partnumbers.txt file has:

Econ:02		- The Number of maps to find/change in Economy level
Powr:08		- The Number of maps to find/change in Power level


---v5.1 changes: ?2/27/13?   ???

Fixed issues in 5.0??
Tune specific file names is possible.


---v4.0
NEED FIND string first!!!!
Find,00000706	; 07 maps to be found of power/economy,  06 maps to be found of economy only
Find,12345678 	; 0x56 - Number of power/economy maps,  0x78 - number of economy only maps


type,01234567:
byte 01: 
byte 23: ECM ID???????not yet
byte 45: string subset ID, all strings searching for same location have same ID.
byte 67: match corresponding map ID, High nibble - ISX/ISL, Fuel/Timing, etc; Low nibble - different maps


MAP File:

type,01234567:

type:
RepB = replace existing map with this map data.
RepW = replace existing map with this map data Integer/Word.
RepL = replace existing map with this map data Long.
AddB = add this data to existing map
AddW = add this data to existing map Integer/Word
AddL = add this data to existing map Long
PerW = Multiply by data and add to original data.

Word 0123: max map value(used for increment maps)
byte 4: 0 - unsigned, 1 - signed
byte 5: 0 - dont change, 1 - Economy Only; 2 - Power/Economy Only; 3 - Both Tuning Levels
byte 67: match corresponding string ID, High nibble - ISX/ISL, Fuel/Timing, etc; Low nibble - different maps


Partnumbers.txt 
v5.2:
 econ and power # of maps to find are now in partnumbers.txt

