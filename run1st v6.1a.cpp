// gasfileapp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <comdef.h>
#include <atlbase.h>
#include <windows.h>
#include <stdio.h>
#include <iostream>
#include <fstream>

using namespace std;

	ofstream  instrout;
	ofstream  mapout(   "maps.bin",			ios::binary);
	ifstream  finfiles( "requiredfiles.txt",ios::binary );
	ifstream  finparts( "partnumbers.txt",	ios::binary);
	ifstream  current;
	ifstream  constread;

//	char compress = 1;
//	int clintcmp( char * first, char * second, unsigned int length );
	void returnpak( unsigned long address1, char * readval, char * retuval, unsigned int length );

	char filename[64];
	int filenamelength;
	char filebuffer[11000];// allows for 500 total files
	long outputaddress;
	int filenumber;
	char databuffer[2056];
	unsigned long filelength1, addrcount, readlength, test;
	char * modfilebuffer, *modpak;
	//char modpak[65535];
	//char modfilebuffer[65535];
	char in;
int _tmain(int argc, _TCHAR* argv[])
{

	char tempbuffer[0x2000];
	char partbuffer[0x6000];
	char file[2056];
	char add[2056];
	char zeroes[256];
	int outputloc;
	int indexloc;
	int fatloc;
	int loop;
	int i,j,k,m,n,x,y;
	int totalparts;
	int totaladdress;
	long address;
//	int numfiles;
	char addy[4];
	unsigned long addr;
//	char temp[16];
	char addresses;
	char files;
	char parts;
	parts = 0;
	totalparts = 0;
	outputloc = 0;
	indexloc = 0;
	fatloc = 0;
	totaladdress = 0;
	instrout.open ("instructions.bin", ios::out | ios::binary);
	instrout.clear();
	for(i = 0;i<16;i++)
		zeroes[i] = 0x00;


	modpak = new char[4098];

	indexloc = 0;
	i = 0;
	instrout.write(zeroes,16);		//reserve space for partnumber count.
	filenumber = 0;
	while(finparts.getline(databuffer,256))//this loop gets all part #'s from partnumbers.txt, and writes 1+8 bytes of partnumber
	{
		if((databuffer[0] == 'e' || databuffer[0] == 'E') && databuffer[1] == 'n' && databuffer[2] == 'd')
		{	//end of file, write number of total parts			
			for(j=0;j<8;j++)
			{
				tempbuffer[j*2] = totalparts>>8;
				tempbuffer[j*2+1] = totalparts&0xFF;
			}
			instrout.seekp(0,ios::beg);
			instrout.write(tempbuffer,16);
			break;
		}
		if((databuffer[0]!= '/' && databuffer[1]!= '/') && (databuffer[0] != ';') && (databuffer[0] != 0x0d) && databuffer[0] != ' ')
		{
			if( (databuffer[0] == 'C' || databuffer[0] == 'c') && databuffer[1] == '0' )	
			{	//C1 CM870
				filename[0] = 1;
				for(i = 1;i<9;i++)
					filename[i] = databuffer[2+i];
			}
			else if( (databuffer[0] == 'C' || databuffer[0] == 'c') && databuffer[1] == '1' )	
			{	//C1 CM871
				filename[0] = 2;
				for(i = 1;i<9;i++)
					filename[i] = databuffer[2+i];
			}
			else if( (databuffer[0] == 'C' || databuffer[0] == 'c') && databuffer[1] == '2' )	
			{	//C2 CM2150
				filename[0] = 3;
				for(i = 1;i<9;i++)
					filename[i] = databuffer[2+i];
			}
			else if( (databuffer[0] == 'C' || databuffer[0] == 'c') && databuffer[1] == '3' )	
			{	//C3 CM2250
				filename[0] = 4;
				for(i = 1;i<9;i++)
					filename[i] = databuffer[2+i];
			}
			else if( (databuffer[0] == 'C' || databuffer[0] == 'c') && databuffer[1] == '4' )	
			{	//C4 ECM Celect
				filename[0] = 7;
				for(i = 1;i<9;i++)
					filename[i] = databuffer[2+i];
			}
			else if( (databuffer[0] == 'C' || databuffer[0] == 'c') && databuffer[1] == '5' )	
			{	//C5 CM2350
				filename[0] = 14;
				for(i = 1;i<9;i++)
					filename[i] = databuffer[2+i];
			}
			else if( (databuffer[0] == 'C' || databuffer[0] == 'c') && databuffer[1] == '6' )	
			{	//C6 CM570
				filename[0] = 15;
				for(i = 1;i<9;i++)
					filename[i] = databuffer[2+i];
			}
			else if((databuffer[0] == 'd' || databuffer[0] == 'D') &&(databuffer[1] == '4' || databuffer[1] == '5'))	
			{	//D4/D5 DDEC 4 & 5
				filename[0] = 5;
				for(i = 1;i<9;i++)
					filename[i] = databuffer[2+i];
			}
			else if( (databuffer[0] == 'd' || databuffer[0] == 'D') && databuffer[1] == '6' )	
			{	//D6 DDEC 6
				filename[0] = 8;
				for(i = 1;i<9;i++)
					filename[i] = databuffer[2+i];
			}
			else if( (databuffer[0] == 'd' || databuffer[0] == 'D') && databuffer[1] == '1' )	
			{	//D1 DDEC 10
				filename[0] = 9;
				for(i = 1;i<9;i++)
					filename[i] = databuffer[2+i];
			}
			else if( (databuffer[0] == 'd' || databuffer[0] == 'D') && databuffer[1] == '3' )	
			{	//D3 DDEC 13
				filename[0] = 10;
				for(i = 1;i<9;i++)
					filename[i] = databuffer[2+i];
			}
			else if( (databuffer[0] == 'p' || databuffer[0] == 'P') && databuffer[1] == '0' )	
			{	//P0 Paccar
				filename[0] = 6;
				for(i = 1;i<9;i++)
					filename[i] = databuffer[2+i];
			}
			else if( (databuffer[0] == 'p' || databuffer[0] == 'P') && databuffer[1] == '1' )	
			{	//P1 Paccar
				filename[0] = 13;
				for(i = 1;i<9;i++)
					filename[i] = databuffer[2+i];
			}
			else if( (databuffer[0] == 'm' || databuffer[0] == 'M') && databuffer[1] == '1' )	
			{	//M1 MaxxForce
				filename[0] = 11;
				for(i = 1;i<9;i++)
					filename[i] = databuffer[2+i];
			}
			else if( (databuffer[0] == 'm' || databuffer[0] == 'M') && databuffer[1] == '2' )	
			{	//M2 MaxxForce
				filename[0] = 12;
				for(i = 1;i<9;i++)
					filename[i] = databuffer[2+i];
			}
			else
				filename[0] = 0;
			if(filename[0] != 0)
			{	
				finparts.getline(databuffer,256);	// Addr: number of address locations
				if((databuffer[0] == 'a' || databuffer[0] == 'A') &&(databuffer[1] == 'd' || databuffer[1] == 'D')&&(databuffer[2] == 'd' || databuffer[2] == 'D')&&(databuffer[3] == 'R' || databuffer[3] == 'r'))
				{	//Addr: line
					for(loop = 0;loop<2;loop++)
					{
						if((databuffer[5+loop]<'0')||(databuffer[5+loop]>'F'))
						{
							cout <<"Incorrect Format1 for Addresses (addr:) in Partnumbers.txt"<<endl;
							cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
							cout <<"Found " << databuffer[5+loop] << endl;
							cout << databuffer << endl;
							while(1);
						}
						if((databuffer[5+loop]<'A')&&(databuffer[5+loop]>'9'))
						{
							cout <<"Incorrect Format2 for Addresses (addr:) in Partnumbers.txt"<<endl;
							cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
							cout <<"Found " << databuffer[5+loop] << endl;
							cout << databuffer << endl;
							while(1);
						}
					}
					asctohex(&databuffer[5],1,&filename[9]);
					addresses = filename[10];
					totaladdress += addresses;
				}
				else
				{
					cout <<"An Error in the Part Numbers occurred Addr:"<<endl;
					while(1);	//error
				}
				finparts.getline(databuffer,256);	//File: number of files
				if((databuffer[0] == 'f' || databuffer[0] == 'F') &&(databuffer[1] == 'i' || databuffer[1] == 'I')&&(databuffer[2] == 'l' || databuffer[2] == 'L')&&(databuffer[3] == 'e' || databuffer[3] == 'E'))
				{	//Files line
					for(loop = 0;loop<2;loop++)
					{
						if((databuffer[5+loop]<'0')||(databuffer[5+loop]>'F'))
						{
							cout <<"Incorrect Format1 for Addresses (File:) in Partnumbers.txt"<<endl;
							cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
							cout <<"Found " << databuffer[5+loop] << endl;
							cout << databuffer << endl;
							while(1);
						}
						if((databuffer[5+loop]<'A')&&(databuffer[5+loop]>'9'))
						{
							cout <<"Incorrect Format2 for Addresses (File:) in Partnumbers.txt"<<endl;
							cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
							cout <<"Found " << databuffer[5+loop] << endl;
							cout << databuffer << endl;
							while(1);
						}
					}
					asctohex(&databuffer[5],1,&filename[10]);
					files = filename[10];
				}
				else
				{
					cout <<"An Error in the Part Numbers occurred File:"<<endl;
					while(1);					//error
				}
				finparts.getline(databuffer,256);	//Econ: number of files
				if((databuffer[0] == 'E' || databuffer[0] == 'e') &&(databuffer[1] == 'C' || databuffer[1] == 'c')&&(databuffer[2] == 'O' || databuffer[2] == 'o')&&(databuffer[3] == 'N' || databuffer[3] == 'n'))
				{
					for(loop = 0;loop<2;loop++)
					{
						if((databuffer[5+loop]<'0')||(databuffer[5+loop]>'F'))
						{
							cout <<"Incorrect Format1 for Addresses (Econ:) in Partnumbers.txt"<<endl;
							cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
							cout <<"Found " << databuffer[5+loop] << endl;
							cout << databuffer << endl;
							while(1);
						}
						if((databuffer[5+loop]<'A')&&(databuffer[5+loop]>'9'))
						{
							cout <<"Incorrect Format2 for Addresses (Econ:) in Partnumbers.txt"<<endl;
							cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
							cout <<"Found " << databuffer[5+loop] << endl;
							cout << databuffer << endl;
							while(1);
						}
					}
					asctohex(&databuffer[5],1,&filename[11]);
				}
				else
				{
					cout <<"An Error in the Part Numbers occurred Econ:"<<endl;
					while(1);					//error
				}
				finparts.getline(databuffer,256);	//Medi: number of files
				if((databuffer[0] == 'M' || databuffer[0] == 'm') &&(databuffer[1] == 'E' || databuffer[1] == 'e')&&(databuffer[2] == 'D' || databuffer[2] == 'd')&&(databuffer[3] == 'I' || databuffer[3] == 'i'))
				{
					for(loop = 0;loop<2;loop++)
					{
						if((databuffer[5+loop]<'0')||(databuffer[5+loop]>'F'))
						{
							cout <<"Incorrect Format1 for Addresses (Medi:) in Partnumbers.txt"<<endl;
							cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
							cout <<"Found " << databuffer[5+loop] << endl;
							cout << databuffer << endl;
							while(1);
						}
						if((databuffer[5+loop]<'A')&&(databuffer[5+loop]>'9'))
						{
							cout <<"Incorrect Format2 for Addresses (Medi:) in Partnumbers.txt"<<endl;
							cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
							cout <<"Found " << databuffer[5+loop] << endl;
							cout << databuffer << endl;
							while(1);
						}
					}
					asctohex(&databuffer[5],1,&filename[12]);
				}
				else
				{
					cout <<"An Error in the Part Numbers occurred Medi:"<<endl;
					while(1);					//error
				}
				finparts.getline(databuffer,256);	//Powr: number of files
				if((databuffer[0] == 'P' || databuffer[0] == 'p') &&(databuffer[1] == 'O' || databuffer[1] == 'o')&&(databuffer[2] == 'W' || databuffer[2] == 'w')&&(databuffer[3] == 'R' || databuffer[3] == 'r'))
				{
					for(loop = 0;loop<2;loop++)
					{
						if((databuffer[5+loop]<'0')||(databuffer[5+loop]>'F'))
						{
							cout <<"Incorrect Format1 for Addresses (Powr:) in Partnumbers.txt"<<endl;
							cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
							cout <<"Found " << databuffer[5+loop] << endl;
							cout << databuffer << endl;
							while(1);
						}
						if((databuffer[5+loop]<'A')&&(databuffer[5+loop]>'9'))
						{
							cout <<"Incorrect Format2 for Addresses (Powr:) in Partnumbers.txt"<<endl;
							cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
							cout <<"Found " << databuffer[5+loop] << endl;
							cout << databuffer << endl;
							while(1);
						}
					}
					asctohex(&databuffer[5],1,&filename[13]);
				}
				else
				{
					cout <<"An Error in the Part Numbers occurred Powr:"<<endl;
					while(1);
					//error
				}

				for(i = 14;i<20;i++)		//two spare bytes
					filename[i] = 0x00;
				instrout.write(filename,20);
				totalparts++;
			}
		}
	}

	if(totaladdress > 500)
	{
		cout <<"You are exceeding the limit of 500 Addresses"<<endl;
		cout<< "total addresses = "<<totaladdress<<endl;
		while(1);
	}
	if(totalparts > 0xff)
	{
		cout <<"You are exceeding the limit of 256 Part Numbers"<<endl;
		cout<< "total parts = "<<totalparts<<endl;
		while(1);
	}
	instrout.seekp(0,ios::end);
	//instrout.write(zeroes,1);

//A - this loop opens all files and writes the 20 byte instruction for each change pertaining to that file
//B - it also writes all of the maps to the second output file.
//C - finally it stores in a table each file name and its number of changes, and the location of where these change instructions were written
	while(finfiles.getline(databuffer,256))		 //requiredfiles.txt								
	{
		if((databuffer[0] == 'e' || databuffer[0] == 'E') && databuffer[1] == 'n' && databuffer[2] == 'd'){
			break;
		}
		if( (databuffer[0]!= '/' && databuffer[1]!= '/') && (databuffer[0] != ';') && (databuffer[0] != 0x0d) && (databuffer[0] != ' ') )
		{		
			for(i=0;i<16;i++)
				filename[i] = 0;	//reset filename to nothing
			i = 0;
			while(databuffer[i] == 0x20 || databuffer[i] == 0x09) i++;	//Parse until find file string, space or tabs are before it.
			n = i;
			while( !(databuffer[i] == 0x20 || databuffer[i] == 0x09 || databuffer[i] == 0x0D || databuffer[i] == 0x0A) )//space,tab,retun
			{
				filename[i-n] = databuffer[i]; //end of name
				i++;
			}
			/*while(databuffer[i] != '.')
			{			
				filename[i] = databuffer[i];
				i++;
			}*/
			for(j = i;j<i;j++)
				filename[j] = databuffer[j];
			filenamelength = i-n;
			address = (long)instrout.tellp();			
			writefile(address);	//opens file found in requiredfiles.txt and parses and write info to map and instruction files.
			filenumber++;
		}
	}

	if(filenumber>500){
		cout <<"You are exceeding the limit of 500 Files"<<endl;
		cout<< "total files= "<<filenumber<<endl;
		while(1);
	}

	finparts.seekg(0,ios::beg);
	finparts.clear();
	i = 0;
	j=0;

// this loop writes finds the files required for each part number, and buffers the files number of changes and location of these changes
// it does this for all x files that and x addresses for each part number
	parts = 0;
	while(finparts.getline(databuffer,256))	//partnumbers.txt
	{
		if((databuffer[0] == 'e' || databuffer[0] == 'E') && databuffer[1] == 'n' && databuffer[2] == 'd')
			break;
		if(i == totalparts)
			break;
		if((databuffer[0]!= '/' && databuffer[1]!= '/') && (databuffer[0] != ';') && (databuffer[0] != 0x0d) && databuffer[0] != ' ')
		{
			if( ((databuffer[0] == 'c' || databuffer[0] == 'C') && (databuffer[1] == '0' || databuffer[1] == '1' || databuffer[1] == '2' || databuffer[1] == '3' || databuffer[1] == '4' || databuffer[1] == '5')) ||	//C0|C1|C2|C3|C4|C5
				((databuffer[0] == 'd' || databuffer[0] == 'D') && (databuffer[1] == '1' || databuffer[1] == '3' || databuffer[1] == '4' || databuffer[1] == '5' || databuffer[1] == '6'))  ||	//D4|D5|D6|D1|D3
				((databuffer[0] ==' p' || databuffer[0] == 'P') && (databuffer[1] == '0' || databuffer[1] == '1')) ||	//P0|P1
				((databuffer[0] ==' m' || databuffer[0] == 'M') && (databuffer[1] == '1' || databuffer[1] == '2'))		//M1|M2
				)
			{
				finparts.getline(databuffer,256);
				if((databuffer[0] == 'a' || databuffer[0] == 'A') &&(databuffer[1] == 'd' || databuffer[1] == 'D')&&(databuffer[2] == 'd' || databuffer[2] == 'D')&&(databuffer[3] == 'r' || databuffer[3] == 'R'))
				{
					asctohex(&databuffer[5],1,&add[parts]);
					addresses = add[parts];
				}
				else
				{
					cout <<"An Error in the Part Numbers occurred Addr:" << endl;
					cout <<"Databuffer = "<< databuffer << endl;
					while(1);
					//error
				}
				finparts.getline(databuffer,256);
				if((databuffer[0] == 'f' || databuffer[0] == 'F') &&(databuffer[1] == 'i' || databuffer[1] == 'I')&&(databuffer[2] == 'l' || databuffer[2] == 'L')&&(databuffer[3] == 'e' || databuffer[3] == 'E'))
				{
					asctohex(&databuffer[5],1,&file[parts]);
					files = file[parts];
					parts++;
				}
				else
				{
					cout <<"An Error in the Part Numbers occurred File:"<<endl;
					cout <<"Databuffer = "<< databuffer << endl;
					while(1);
					//error
				}
				finparts.getline(databuffer,256);	//Econ # of maps
				finparts.getline(databuffer,256);	//Medium # of maps
				finparts.getline(databuffer,256);	//Power # of maps
				for(k=0;k<addresses;k++)
				{	
					m=0;
					finparts.getline(databuffer,256);
					while(databuffer[m] != 0x20  && databuffer[m] != 0x09) m++; //look for space and/or tab
					while(databuffer[m] == 0x20 || databuffer[m] == 0x09) m++;	//Parse until find end of space and/or tab
					asctohex(&databuffer[m],4,&partbuffer[j*6]);
					while(databuffer[m] != 0x20  && databuffer[m] != 0x09) m++; //look for space and/or tab
					while(databuffer[m] == 0x20 || databuffer[m] == 0x09) m++;	//Parse until find space and/or tab, return 
					partbuffer[j*6+4] = 0;
					if(databuffer[m] == 0x0D)
						partbuffer[j*6+5] = 0;
					else
						asctohex(&databuffer[m],1,&partbuffer[j*6+5]);
					j++;
				}
				for(k=0;k<files;k++)
				{
					m = 0;
					finparts.getline(databuffer,256);
					while(databuffer[m] != 0x20  && databuffer[m] != 0x09) m++; //look for space and/or tab
					while(databuffer[m] == 0x20 || databuffer[m] == 0x09) m++;	//Parse until find file string
					n = m;	//start of name
					while( !(databuffer[m] == 0x20 || databuffer[m] == 0x09 || databuffer[m] == 0x0D || databuffer[m] == 0x0A) )//space,tab,retun
					{
						filename[m-n] = databuffer[m]; //end of name
						m++;
					}
					/*while(databuffer[m] != '.')
					{			
						filename[m-n] = databuffer[m];
						m++;
					}
					filename[m-n] = databuffer[m];
					m++;
					filename[m-n] = databuffer[m];
					m++;
					filename[m-n] = databuffer[m];
					m++;
					filename[m-n] = databuffer[m];
					m++;*/
					m-=n;
					filenamelength = m;
					/*if(m == 12)
					{
						filename[4] = '.';
						filename[5] = 'T';
					}
					if(m == 13)
						filename[5] = '.';*/
					//now search for the filename in the filebuffer
					n=0;
					while(n<=filenumber)
					{
						for(x=0;x<filenamelength;x++)
							databuffer[x] = filebuffer[n*22+x];	//File name
						for(y=x;y<16;y++)
							databuffer[y] = 0;
						if (strncmp (databuffer,filename,filenamelength) == 0)
						{
							partbuffer[j*6]   = filebuffer[n*22+16];	//Files address and number of maps to be found there.
							partbuffer[j*6+1] = filebuffer[n*22+17];
							partbuffer[j*6+2] = filebuffer[n*22+18];
							partbuffer[j*6+3] = filebuffer[n*22+19];
							partbuffer[j*6+4] = filebuffer[n*22+20];
							partbuffer[j*6+5] = filebuffer[n*22+21];
							j++;
							n = filenumber+4;
						}
						else
							n++;
					}
					if(filenamelength > 16)
					{
						cout << "Filename \"" << filename << "\" is too long.  Please limit names to 16 characters total.(1)" << endl;
						while(1);
					}
					if(n==filenumber+1)
					{					
						cout <<"An Error(1) has occured, the file not found is " << filename<<endl;
						while(1);
					}
				}
				i++;
			}
		}
	}

// this step now writes the info for the x addresses, and y files for each pn, and then writes the address of this info to the part number table
//now write each partnumbers stuff out and write the address in the pn index
	instrout.seekp(0,ios::end);
	instrout.clear();
	address = (long)instrout.tellp();
	i = 0;
	for(j=0;j<totalparts;j++)
	{
		k = add[j];
		x = file[j];
		k+=x;
		i+=k;
	}
	instrout.write(partbuffer,i*6);
	i = 0x20;	//first location for jump address of first entry
	for(j=0;j<totalparts;j++)
	{
		addr = address;
		instrout.seekp(i);
//		instrout.clear();
		addy[3] = addr & 0x000000ff;
		addr = addr/256;
		addy[2] = addr & 0x000000ff;
		addr = addr/256;
		addy[1] = addr & 0x000000ff;
		addr = addr/256;
		addy[0] = addr & 0x000000ff;
		instrout.write(addy,4);
		i+=20;		//Length of each partnumber entry
		k = add[j];
		x =file[j];
		k+=x;
		k =6*k;
		address+=k;
	}	
instrout.clear();
instrout.close();
//cout<<"done"<<endl;
//cin>>in;
	return 0;
}
void writefile(long add)
{
	char writebuffer[0x20000];
	char change[12];
	char addy[4];
	unsigned long addr;
	char zeroes[16];
	int i,j,loop;//,temp,temp1;
	int sandrs;
	unsigned int bytes;
	int check;
	unsigned char isValid;
	j=filenumber*22; 
	for(i = 0;i<16;i++)
		zeroes[i] = 0x00;
	for(i = 0;i<filenamelength;i++)
		filebuffer[j+i] = filename[i];
	sandrs = 0;
	addr = add;
	addy[3] = addr & 0x000000ff;
	addr = addr/256;
	addy[2] = addr & 0x000000ff;
	addr = addr/256;
	addy[1] = addr & 0x000000ff;
	addr = addr/256;
	addy[0] = addr & 0x000000ff;
	for(i = 0;i<4;i++)
		filebuffer[j+16+i] = addy[i]; 
	
	current.open(filename, ios::binary);	//Open the file found in requiredfiles.txt for parsing
	current.clear();
	current.seekg(0,ios::end);
	check = (int)current.tellg();
	current.close();
	if((check == 0)||(check == 0xffffffff)){
		cout <<"An Error(2) has occured file not found "<<filename<<endl;
		while(1);
	}
	current.open(filename,ios::binary);
	current.clear();

	while(current.getline(databuffer,256))	
	{
		if( (databuffer[0] == 'e' || databuffer[0] == 'E') &&	//End
			(databuffer[1] == 'n' || databuffer[1] == 'N') &&
			(databuffer[2] == 'd' || databuffer[2] == 'D') )
		{
			filebuffer[j+20] = sandrs/256;
			filebuffer[j+21] = sandrs%256;
			break;
		}
		if((databuffer[0]!= '/' && databuffer[1]!= '/') && (databuffer[0] != ';') && (databuffer[0] != 0x0D) && databuffer[0] != ' ')
		{
			if( (databuffer[0] == 'C'||databuffer[0]=='c') && databuffer[1] == 'h' && databuffer[2] == 'n' && databuffer[3] == 'g' )
			{
				cout <<"An Error in the file line 436 has occured"<<endl;
				cout <<"Using old keyword Chng"<<endl;
				cout <<"Code 97 "<<endl;
				cout<<filename<<endl;
				cout<<filebuffer<<endl;
				cout<<databuffer<<endl;
				cout<<"databuffer[0]= "<<databuffer[0]<<endl;
				cout<<"databuffer[3]= "<<databuffer[3]<<endl;
				while(1);
			}
			if(	(databuffer[0] == 'F' && databuffer[1] == 'i' && databuffer[2] == 'n' && databuffer[3] == 'd') ||	//Find	- Number of search strings to find
				(databuffer[0] == 'S' && databuffer[1] == 'r' && databuffer[2] == 'c' && databuffer[3] == 'h') ||	//srch	- Search strings to be found
				(databuffer[0] == 'R' && databuffer[1] == 'e' && databuffer[2] == 'p' && databuffer[3] == 'B') ||	//Rep - Replace Byte
				(databuffer[0] == 'A' && databuffer[1] == 'd' && databuffer[2] == 'd' && databuffer[3] == 'B') ||	//Add - Add Byte
				(databuffer[0] == 'P' && databuffer[1] == 'e' && databuffer[2] == 'r' && databuffer[3] == 'B')		//Per - Percent Byte
				)
			{
				bytes = 0;
				for(loop = 0;loop<8;loop++)
				{
					if((databuffer[5+loop]<'0')||(databuffer[5+loop]>'F'))
					{
						cout <<"Incorrect Format1 for CHNG Address in "<<filename<<endl;
						cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
						cout <<"Found " << databuffer[5+loop] << endl;
						cout << databuffer << endl;
						while(1);
					}
					if((databuffer[5+loop]<'A')&&(databuffer[5+loop]>'9'))
					{
						cout <<"Incorrect Format2 for CHNG Address in "<<filename<<endl;
						cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
						cout <<"Found " << databuffer[5+loop] << endl;
						cout << databuffer << endl;
						while(1);
					}
				}
				asctohex(&databuffer[5],4,addy);	//8 byte address (after Chng,) OR map ID
				sandrs++;
				change[0] = 0;//change type;
				if( databuffer[0] == 'F' && databuffer[1] == 'i' && databuffer[2] == 'n' && databuffer[3] == 'd' )//change type - find = number of finds
				{	
					change[1] = -1;
				}
				else if( databuffer[0] == 'S' )//change type, Search string
					change[1] = 0;
				else if( databuffer[0] == 'R' && databuffer[3] == 'B' )	//change type, replace byte
					change[1] = 0x11;
				else if( databuffer[0] == 'A' && databuffer[3] == 'B' )	//change type, add byte
					change[1] = 0x21;
				else if( databuffer[0] == 'P' && databuffer[3] == 'B' )	//change type, percent byte
					change[1] = 0x31;
				else
				{
					cout <<"An Error in the file line 471 has occured"<<endl;
					cout <<"Code 99 "<<endl;
					cout<<filename<<endl;
					cout<<databuffer<<endl;
					cout<<"databuffer[0]= "<<databuffer[0]<<endl;
					cout<<"databuffer[3]= "<<databuffer[3]<<endl;
					while(1);
				}
					
				change[2] = 0;//change amnt;
				change[3] = 0;//change amnt;
				change[4] = 0;//start address -OR- map/search string ID
				change[8] = 0;//start flash address;
				for(i = 0;i<4;i++)
					change[4+i] = addy[i];
				while(current.getline(databuffer,256))	//Grab map/search strings, line by line
				{
					if(databuffer[0] == 0x0D || databuffer[0] == ' ')	//end of map/string(line with space/return at start)
					{
						change[2] = (bytes/256) & 0x00ff;	//number of bytes collected from map
						change[3] = bytes & 0x00ff;

						addr = filelength1;			//not sure where filelength1 comes from	

						if( test == 0 )				//test???
							addr =(unsigned long) mapout.tellp();	//mappout is the index where it is at currently...

						mapout.clear();
						mapout.close();

						if(addr == 0xFFFFFFFF)
						{
							cout <<"An Error in the file line 558 has occured"<<endl;
							cout <<"Is the map.bin file open in another program?"<<endl;
							cout <<"Code 92 "<<endl;
							cout<<filename<<endl;
							cout<<databuffer<<endl;
							cout<<"databuffer[0]= "<<databuffer[0]<<endl;
							cout<<"databuffer[3]= "<<databuffer[3]<<endl;
							while(1);
						}
							

						constread.open("maps.bin",ios::binary);  //open map file to look for same data map.
						constread.seekg(ios::beg);

						filelength1 = addr;
						addrcount = 0;		//increment through file to search for same map
						test = 0;	//get addr from mapout
						if( (bytes < 4098) && (filelength1>bytes) )//& (compress==1) ) 
						{							
							modfilebuffer = new char[filelength1];
							constread.read(modfilebuffer,filelength1);
							while( addrcount < (filelength1-bytes) )	//don't search for maps farther than file size
							{
//cout<<"RETURNPAK addrcount="<<addrcount<<"; bytes="<<bytes<<"; filelength="<<filelength1<<"; addr="<<addr<<endl;
								returnpak( addrcount, modfilebuffer, modpak, bytes );
								if( !memcmp( writebuffer, modpak, bytes ) )
								{
									addr = addrcount;
									test = 1;	
								}
								addrcount++;	//next byte is start of new possible matching map
							}
							delete modfilebuffer;
						}
						else	//map is bigger than 4098
						{
							addrcount = 0;	//
							test = 0; 		//			
						}
						constread.clear();
						constread.close();

						mapout.open("maps.bin",ios::app|ios::binary);
						mapout.clear();
						
						addy[3] = addr & 0x000000ff;
						addr = addr/256;
						addy[2] = addr & 0x000000ff;
						addr = addr/256;
						addy[1] = addr & 0x000000ff;
						addr = addr/256;
						addy[0] = addr & 0x000000ff;
						for(i = 0;i<3;i++)
							change[9+i] = addy[i+1]; 
						instrout.write(change,12);			//Write 12 byte instruction to instruction file

						addr = filelength1;

						if( test == 0)
							mapout.write(writebuffer,bytes);
						
						break;
					}
					else
					{
						i = 0;
						while( (databuffer[i] != 0x0d) && (databuffer[i]!=' ') && (databuffer[i] != ';') && (databuffer[i] != '/') )	//Return & Space -- End of Line
						{
							if(databuffer[i] == ',')
								i++;
							else
							{		
								isValid=1;
								for(char j=0; j<2 ; j++){
									if( !((databuffer[i+j] >= '0' && databuffer[i+j] <= '9') || //valid entry - number
										  (databuffer[i+j] >= 'A' && databuffer[i+j] <= 'F') ||	//valid entry - Cap A-F
										  (databuffer[i+j] >= 'a' && databuffer[i+j] <= 'f')) )	//valid entry - lower a-f
									{
										isValid=0;
									}
								}
								/*
								if(databuffer[i+2] != ',')
								{
								*/
									if( isValid ) //valid number
									{
										asctohex(&databuffer[i],1,&writebuffer[bytes]);
										i++;
										i++;
										bytes++;
									}
									else
									{
										cout <<"An Error in the files occured CHNG "<<endl;
										cout <<"Code 100 "<<endl;
										cout<<filename<<endl;
										cout<<databuffer<<endl;
										cout<<i<<"=i, databuffer[i]= "<<databuffer[i]<<endl;
										cout<<i+1<<"=i+1, databuffer[i+1]= "<<databuffer[i+1]<<endl;
										cout<<i+2<<"=i+2, databuffer[i+2]= "<<databuffer[i+2]<<endl;
										cout<<i+3<<"=i+3, databuffer[i+3]= "<<databuffer[i+3]<<endl;
										while(1);
									}
								/*
								}
								else
								{
									asctohex(&databuffer[i],1,&writebuffer[bytes]);
									i++;
									i++;
									bytes++;
								}
								*/
							}
						}
					}
				}
			}
			if(	(databuffer[0] == 'R' && databuffer[1] == 'e' && databuffer[2] == 'p' && databuffer[3] == 'W') ||	//Rep - Replace Word
				(databuffer[0] == 'A' && databuffer[1] == 'd' && databuffer[2] == 'd' && databuffer[3] == 'W') ||	//Add - Add Word
				(databuffer[0] == 'P' && databuffer[1] == 'e' && databuffer[2] == 'r' && databuffer[3] == 'W')		//Per - Percent Word
				)
			{
				bytes = 0;
				for(loop = 0;loop<8;loop++)
				{
					if((databuffer[5+loop]<'0')||(databuffer[5+loop]>'F'))
					{
						cout <<"Incorrect Format1 for Word Address in "<<filename<<endl;
						cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
						cout <<"Found " << databuffer[5+loop] << endl;
						cout << databuffer << endl;
						while(1);
					}
					if((databuffer[5+loop]<'A')&&(databuffer[5+loop]>'9'))
					{
						cout <<"Incorrect Format2 for Word Address in "<<filename<<endl;
						cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
						cout <<"Found " << databuffer[5+loop] << endl;
						cout << databuffer << endl;
						while(1);
					}
				}
				asctohex(&databuffer[5],4,addy);	//8 byte map ID/address (after AddW/RepW)
				sandrs++;
				change[0] = 0;//change type;
				if(		 databuffer[0] == 'R' && databuffer[3] == 'W' )	//change type, replace word
					change[1] = 0x12;
				else if( databuffer[0] == 'A' && databuffer[3] == 'W' )	//change type, add word
					change[1] = 0x22;
				else if( databuffer[0] == 'P' && databuffer[3] == 'W' )	//change type, percent word
					change[1] = 0x32;
				else{
					cout <<"An Error in the file line 869 has occured"<<endl;
					cout <<"Code 94 "<<endl;
					cout<<filename<<endl;
					cout<<databuffer<<endl;
					cout<<"databuffer[0]= "<<databuffer[0]<<endl;
					cout<<"databuffer[3]= "<<databuffer[3]<<endl;
					while(1);
				}
				change[2] = 0;//change amnt;
				change[3] = 0;//change amnt;
				change[4] = 0;//start address;
				change[8] = 0;//start flash address;
				for(i = 0;i<4;i++)
					change[4+i] = addy[i];
				while(current.getline(databuffer,256))
				{
					if(databuffer[0] == 0x0D || databuffer[0] == ' ')	//end of map/string(line with space/return at start)
					{
						change[2] = (bytes/256) & 0x00ff;	//number of bytes collected from map
						change[3] = bytes & 0x00ff;

						addr = filelength1;			//not sure where filelength1 comes from	

						if( test == 0 )				//test???
							addr =(unsigned long) mapout.tellp();	//mappout is the index where it is at currently...

						mapout.clear();
						mapout.close();

						constread.open("maps.bin",ios::binary);  //open map file to look for same data map.
						constread.seekg(ios::beg);

						filelength1 = addr;
						addrcount = 0;		//increment through file to search for same map
						test = 0;	//get addr from mapout
						if( (bytes < 4098) && (filelength1>bytes) )//& (compress==1) ) 
						{							
							modfilebuffer = new char[filelength1];
							constread.read(modfilebuffer,filelength1);
							while( addrcount < (filelength1-bytes) )	//don't search for maps farther than file size
							{
								returnpak( addrcount, modfilebuffer, modpak, bytes );
								if( !memcmp( writebuffer, modpak, bytes ) )
								{
									addr = addrcount;
									test = 1;	
								}
								addrcount++;	//next byte is start of new possible matching map
							}
							delete modfilebuffer;
						}
						else	//map is bigger than 4098
						{
							addrcount = 0;	//
							test = 0; 		//			
						}
						constread.clear();
						constread.close();

						mapout.open("maps.bin",ios::app|ios::binary);
						mapout.clear();
						
						addy[3] = addr & 0x000000ff;
						addr = addr/256;
						addy[2] = addr & 0x000000ff;
						addr = addr/256;
						addy[1] = addr & 0x000000ff;
						addr = addr/256;
						addy[0] = addr & 0x000000ff;
						for(i = 0;i<3;i++)
							change[9+i] = addy[i+1]; 
						instrout.write(change,12);			//Write 12 byte instruction to instruction file

						addr = filelength1;

						if( test == 0)
							mapout.write(writebuffer,bytes);
						
						break;
					}
					else
					{
						i = 0;
						while( (databuffer[i] != 0x0d) && (databuffer[i]!=' '))	//Return & Space
						{
							if(databuffer[i] == ',')
								i++;
							else
							{		
								isValid=1;
								for(char j=0; j<4 ; j++){
									if( !((databuffer[i+j] >= '0' && databuffer[i+j] <= '9') || //valid entry - number
										  (databuffer[i+j] >= 'A' && databuffer[i+j] <= 'F') ||	//valid entry - Cap A-F
										  (databuffer[i+j] >= 'a' && databuffer[i+j] <= 'f')) )	//valid entry - lower a-f
									{
										isValid=0;
									}
								}
								if( isValid ) //valid number
								{
									asctohex(&databuffer[i],2,&writebuffer[bytes]);
									i+=4;
									bytes+=2;
								}
								else
								{
									cout <<"An Error in the files occured WORD "<<endl;
									cout <<"Code 102 "<<endl;
									cout<<filename<<endl;
									cout<<databuffer<<endl;
									cout<<i<<"=i, databuffer[i]= "<<databuffer[i]<<endl;
									cout<<i+1<<"=i+1, databuffer[i+1]= "<<databuffer[i+1]<<endl;
									cout<<i+2<<"=i+2, databuffer[i+2]= "<<databuffer[i+2]<<endl;
									cout<<i+3<<"=i+3, databuffer[i+3]= "<<databuffer[i+3]<<endl;
									cout<<i+4<<"=i+4, databuffer[i+4]= "<<databuffer[i+4]<<endl;
									while(1);
								}
							}
						}
					}
				}
			}
			if(	(databuffer[0] == 'R' && databuffer[1] == 'e' && databuffer[2] == 'p' && databuffer[3] == 'L') ||	//Rep - Replace Long
				(databuffer[0] == 'A' && databuffer[1] == 'd' && databuffer[2] == 'd' && databuffer[3] == 'L') ||	//Add - Add Long
				(databuffer[0] == 'P' && databuffer[1] == 'e' && databuffer[2] == 'r' && databuffer[3] == 'L')		//Per - Percent Long
				)
			{
				bytes = 0;
				for(loop = 0;loop<8;loop++)
				{
					if((databuffer[5+loop]<'0')||(databuffer[5+loop]>'F'))
					{
						cout <<"Incorrect Format1 for Long Address in "<<filename<<endl;
						cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
						cout <<"Found " << databuffer[5+loop] << endl;
						cout << databuffer << endl;
						while(1);
					}
					if((databuffer[5+loop]<'A')&&(databuffer[5+loop]>'9'))
					{
						cout <<"Incorrect Format2 for Long Address in "<<filename<<endl;
						cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
						cout <<"Found " << databuffer[5+loop] << endl;
						cout << databuffer << endl;
						while(1);
					}
				}
				asctohex(&databuffer[5],4,addy);	//8 byte map ID/address (after AddL/RepL)
				sandrs++;
				change[0] = 0;//change type;
				if(		 databuffer[0] == 'R' && databuffer[3] == 'L' )	//change type, replace long
					change[1] = 0x14;
				else if( databuffer[0] == 'A' && databuffer[3] == 'L' )	//change type, add long
					change[1] = 0x24;
				else if( databuffer[0] == 'P' && databuffer[3] == 'L' )	//change type, percent Long
					change[1] = 0x34;
				else{
					cout <<"An Error in the file line 1026 has occured"<<endl;
					cout <<"Code 95 "<<endl;
					cout<<filename<<endl;
					cout<<databuffer<<endl;
					cout<<"databuffer[0]= "<<databuffer[0]<<endl;
					cout<<"databuffer[3]= "<<databuffer[3]<<endl;
					while(1);
				}
				change[2] = 0;//change amnt;
				change[3] = 0;//change amnt;
				change[4] = 0;//start address;
				change[8] = 0;//start flash address;
				for(i = 0;i<4;i++)
					change[4+i] = addy[i];
				while(current.getline(databuffer,256))
				{
					if(databuffer[0] == 0x0D || databuffer[0] == ' ')	//end of map/string(line with space/return at start)
					{
						change[2] = (bytes/256) & 0x00ff;	//number of bytes collected from map
						change[3] = bytes & 0x00ff;

						addr = filelength1;			//not sure where filelength1 comes from	

						if( test == 0 )				//test???
							addr =(unsigned long) mapout.tellp();	//mappout is the index where it is at currently...

						mapout.clear();
						mapout.close();

						constread.open("maps.bin",ios::binary);  //open map file to look for same data map.
						constread.seekg(ios::beg);

						filelength1 = addr;
						addrcount = 0;		//increment through file to search for same map
						test = 0;	//get addr from mapout
						if( (bytes < 4098) && (filelength1>bytes) )//& (compress==1) ) 
						{							
							modfilebuffer = new char[filelength1];
							constread.read(modfilebuffer,filelength1);
							while( addrcount < (filelength1-bytes) )	//don't search for maps farther than file size
							{
								returnpak( addrcount, modfilebuffer, modpak, bytes );
								if( !memcmp( writebuffer, modpak, bytes ) )
								{
									addr = addrcount;
									test = 1;	
								}
								addrcount++;	//next byte is start of new possible matching map
							}
							delete modfilebuffer;
						}
						else	//map is bigger than 4098
						{
							addrcount = 0;	//
							test = 0; 		//			
						}
						constread.clear();
						constread.close();

						mapout.open("maps.bin",ios::app|ios::binary);
						mapout.clear();
						
						addy[3] = addr & 0x000000ff;
						addr = addr/256;
						addy[2] = addr & 0x000000ff;
						addr = addr/256;
						addy[1] = addr & 0x000000ff;
						addr = addr/256;
						addy[0] = addr & 0x000000ff;
						for(i = 0;i<3;i++)
							change[9+i] = addy[i+1]; 
						instrout.write(change,12);			//Write 12 byte instruction to instruction file

						addr = filelength1;

						if( test == 0)
							mapout.write(writebuffer,bytes);
						
						break;
					}
					else
					{
						i = 0;
						while( (databuffer[i] != 0x0d) && (databuffer[i]!=' '))	//Return & Space
						{
							if(databuffer[i] == ',')
								i++;
							else
							{
								int length = 8;
								isValid=1;
								if(change[1] == 0x34)	//signed int, not long
									length = 4;
								for(char j=0; j<length ; j++){
									if( !((databuffer[i+j] >= '0' && databuffer[i+j] <= '9') || //valid entry - number
										  (databuffer[i+j] >= 'A' && databuffer[i+j] <= 'F') ||	//valid entry - Cap A-F
										  (databuffer[i+j] >= 'a' && databuffer[i+j] <= 'f')) )	//valid entry - lower a-f
									{
										isValid=0;
									}
								}
								if( isValid ) //valid number
								{
										asctohex(&databuffer[i],length/2,&writebuffer[bytes]);
										i+=length;
										bytes+=length/2;
								}
								else
								{
									cout <<"An Error in the files occured LONG "<<endl;
									cout <<"Code 103 "<<endl;
									cout<<filename<<endl;
									cout<<databuffer<<endl;
									cout<<i<<"=i, databuffer[i]= "<<databuffer[i]<<endl;
									cout<<i+1<<"=i+1, databuffer[i+1]= "<<databuffer[i+1]<<endl;
									cout<<i+2<<"=i+2, databuffer[i+2]= "<<databuffer[i+2]<<endl;
									cout<<i+3<<"=i+3, databuffer[i+3]= "<<databuffer[i+3]<<endl;
									cout<<i+4<<"=i+4, databuffer[i+4]= "<<databuffer[i+4]<<endl;
									cout<<i+5<<"=i+5, databuffer[i+5]= "<<databuffer[i+5]<<endl;
									cout<<i+6<<"=i+6, databuffer[i+6]= "<<databuffer[i+6]<<endl;
									cout<<i+7<<"=i+7, databuffer[i+7]= "<<databuffer[i+7]<<endl;
									cout<<i+8<<"=i+8, databuffer[i+8]= "<<databuffer[i+8]<<endl;
									while(1);
								}
							}
						}
					}
				}
			}
			if(	(databuffer[0] == 'R' && databuffer[1] == 'e' && databuffer[2] == 'p' && databuffer[3] == 'F') ||	//Rep - Replace Float
				(databuffer[0] == 'A' && databuffer[1] == 'd' && databuffer[2] == 'd' && databuffer[3] == 'F') ||	//Add - Add Float
				(databuffer[0] == 'P' && databuffer[1] == 'e' && databuffer[2] == 'r' && databuffer[3] == 'F')		//Per - Percent Float
				)
			{
				bytes = 0;
				for(loop = 0;loop<8;loop++)
				{
					if((databuffer[5+loop]<'0')||(databuffer[5+loop]>'F'))
					{
						cout <<"Incorrect Format1 for Float Address in "<<filename<<endl;
						cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
						cout <<"Found " << databuffer[5+loop] << endl;
						cout << databuffer << endl;
						while(1);
					}
					if((databuffer[5+loop]<'A')&&(databuffer[5+loop]>'9'))
					{
						cout <<"Incorrect Format2 for Float Address in "<<filename<<endl;
						cout <<"Looking for a hex number at " << loop << " + 5"<<endl;
						cout <<"Found " << databuffer[5+loop] << endl;
						cout << databuffer << endl;
						while(1);
					}
				}
				asctohex(&databuffer[5],4,addy);	//8 byte map ID/address (after AddL/RepL)
				sandrs++;
				change[0] = 0;//change type;
				if(		 databuffer[0] == 'R' )	//change type, replace float
					change[1] = 0x18;
				else if( databuffer[0] == 'A' )	//change type, add float
					change[1] = 0x28;
				else if( databuffer[0] == 'P' )	//change type, percent float
					change[1] = 0x38;
				else{
					cout <<"An Error in the file line 1187 has occured"<<endl;
					cout <<"Code 96 "<<endl;
					cout<<filename<<endl;
					cout<<databuffer<<endl;
					cout<<"databuffer[0]= "<<databuffer[0]<<endl;
					cout<<"databuffer[3]= "<<databuffer[3]<<endl;
					while(1);
				}
				change[2] = 0;//change amnt;
				change[3] = 0;//change amnt;
				change[4] = 0;//start address;
				change[8] = 0;//start flash address;
				for(i = 0;i<4;i++)
					change[4+i] = addy[i];
				while(current.getline(databuffer,256))
				{
					if(databuffer[0] == 0x0D || databuffer[0] == ' ')	//end of map/string(line with space/return at start)
					{
						change[2] = (bytes/256) & 0x00ff;	//number of bytes collected from map
						change[3] = bytes & 0x00ff;

						addr = filelength1;			//not sure where filelength1 comes from	

						if( test == 0 )				//test???
							addr =(unsigned long) mapout.tellp();	//mappout is the index where it is at currently...

						mapout.clear();
						mapout.close();

						constread.open("maps.bin",ios::binary);  //open map file to look for same data map.
						constread.seekg(ios::beg);

						filelength1 = addr;
						addrcount = 0;		//increment through file to search for same map
						test = 0;	//get addr from mapout
						if( (bytes < 4098) && (filelength1>bytes) )//& (compress==1) ) 
						{							
							modfilebuffer = new char[filelength1];
							constread.read(modfilebuffer,filelength1);
							while( addrcount < (filelength1-bytes) )	//don't search for maps farther than file size
							{
								returnpak( addrcount, modfilebuffer, modpak, bytes );
								if( !memcmp( writebuffer, modpak, bytes ) )
								{
									addr = addrcount;
									test = 1;	
								}
								addrcount++;	//next byte is start of new possible matching map
							}
							delete modfilebuffer;
						}
						else	//map is bigger than 4098
						{
							addrcount = 0;	//
							test = 0; 		//			
						}
						constread.clear();
						constread.close();

						mapout.open("maps.bin",ios::app|ios::binary);
						mapout.clear();
						
						addy[3] = addr & 0x000000ff;
						addr = addr/256;
						addy[2] = addr & 0x000000ff;
						addr = addr/256;
						addy[1] = addr & 0x000000ff;
						addr = addr/256;
						addy[0] = addr & 0x000000ff;
						for(i = 0;i<3;i++)
							change[9+i] = addy[i+1]; 
						instrout.write(change,12);			//Write 12 byte instruction to instruction file

						addr = filelength1;

						if( test == 0)
							mapout.write(writebuffer,bytes);
						
						break;
					}
					else
					{
						i = 0;
						while( (databuffer[i] != 0x0d) && (databuffer[i]!=' '))	//Return & Space
						{
							if(databuffer[i] == ',')
								i++;
							else
							{
								int length = 8;
								isValid=1;
								if(change[1] == 0x38)	//signed int, not float
									length = 4;
								for(char j=0; j<length ; j++){
									if( !((databuffer[i+j] >= '0' && databuffer[i+j] <= '9') || //valid entry - number
										  (databuffer[i+j] >= 'A' && databuffer[i+j] <= 'F') ||	//valid entry - Cap A-F
										  (databuffer[i+j] >= 'a' && databuffer[i+j] <= 'f')) )	//valid entry - lower a-f
									{
										isValid=0;
									}
								}
								if( isValid ) //valid number
								{
										asctohex(&databuffer[i],length/2,&writebuffer[bytes]);
										i+=length;
										bytes+=length/2;
								}
								else
								{
									cout <<"An Error in the files occured FLOAT "<<endl;
									cout <<"Code 104 "<<endl;
									cout<<filename<<endl;
									cout<<databuffer<<endl;
									cout<<change[1]<<endl;
									cout<<i<<"=i, databuffer[i]= "<<databuffer[i]<<endl;
									cout<<i+1<<"=i+1, databuffer[i+1]= "<<databuffer[i+1]<<endl;
									cout<<i+2<<"=i+2, databuffer[i+2]= "<<databuffer[i+2]<<endl;
									cout<<i+3<<"=i+3, databuffer[i+3]= "<<databuffer[i+3]<<endl;
									cout<<i+4<<"=i+4, databuffer[i+4]= "<<databuffer[i+4]<<endl;
									cout<<i+5<<"=i+5, databuffer[i+5]= "<<databuffer[i+5]<<endl;
									cout<<i+6<<"=i+6, databuffer[i+6]= "<<databuffer[i+6]<<endl;
									cout<<i+7<<"=i+7, databuffer[i+7]= "<<databuffer[i+7]<<endl;
									cout<<i+8<<"=i+8, databuffer[i+8]= "<<databuffer[i+8]<<endl;
									while(1);
								}
							}
						}
					}
				}
			}
		}
	}
	current.close();
}
void asctohex(char *ascvalue, unsigned int bytes,char *hexvalue)
{
	unsigned char temp_byte,temp_byte1;
	unsigned int l;
	for(l = 0;l<bytes;l++)
	{
		temp_byte = *ascvalue;
		ascvalue++;
		temp_byte1 = *ascvalue;
		ascvalue++;
		if(temp_byte >= 0x30 && temp_byte <0x3a)
			temp_byte -= 0x30;
		else
		if(temp_byte >0x40 && temp_byte < 0x47)
			temp_byte -= 0x37;
		else
		if(temp_byte > 0x60 && temp_byte <0x67)
			temp_byte -= 0x57;
		if(temp_byte1 >= 0x30 && temp_byte1 <0x3a)
			temp_byte1 -= 0x30;
		else
		if(temp_byte1 >0x40 && temp_byte1 < 0x47)
			temp_byte1 -= 0x37;
		else
		if(temp_byte1 > 0x60 && temp_byte1 <0x67)
			temp_byte1 -= 0x57;
		*hexvalue = ((temp_byte*16) + temp_byte1);
		hexvalue++;
	}
}


void returnpak( unsigned long address1, char * readval, char * retuval, unsigned int length )
{
	for( unsigned int i = 0 ; i < length ; i++ )
		retuval[i] = readval[address1 + i];
	retuval[length] = 0;
}


